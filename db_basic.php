<?php

echo "<pre>";

//print_r(PDO::getAvailableDrivers());

$host = "localhost";
$dbname = "atomic_project_b37";
$user = "root";
$pass = "";

try {

# MySQL with PDO_MYSQL
    $DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
    $DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
    $STH = $DBH->prepare("select * from book_title");
    $STH->execute();

    echo "successfully connected <br>";
}



catch (PDOException $e){

    echo "I'm sorry";
    file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
}



/*
$data = array('book_title'=>'Book Title 1', 'author_name'=>'Rahim');

$STH1 = $DBH->prepare("insert into book_title (book_title, author_name) value (:book_title, :author_name)");
$STH1->execute($data);

*/


$STH->setFetchMode(PDO::FETCH_ASSOC);

while($row = $STH->fetch()){

    echo $row['id'] . "\n <br>";

    echo $row['book_title'] . "\n <br>";

    echo $row['author_name'] . "\n <br>";

}

$STH->execute();

$STH->setFetchMode(PDO::FETCH_OBJ);

while($row = $STH->fetch()){

    echo $row->id . "\n <br>";

    echo $row->book_title . "\n <br>";

    echo $row->author_name . "\n <br>";

}


//catch(PDOException $e){
//    echo $e->getMessage();
//}


